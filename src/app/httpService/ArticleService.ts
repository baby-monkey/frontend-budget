import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Article } from '../model/article';
import { HttpClientService } from './http-client.service';
import { of, Observable } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ArticleService extends HttpClientService<Article> {
  constructor(http: HttpClient) {
    super(http);
    this.setObjectName(Article.name);
  }
  search(term: string): Observable<Article[]> {
    if (!term.trim()) 
      return of([]);
    
    const url = `${this.baseUrl}/${this.objName}`;
    return this.http.get<Article[]>(`${url}/?label=${term}`).pipe(
        tap(x => x.length ?
           this.log(`Search found matching ${this.objName} for ${term}`) :
           this.log(`Search found no matching ${this.objName} for ${term}`)),
        catchError(this.handleError<Article[]>(`Search for ${term} in ${this.objName} from ${url}`))
      );
  }
}
