import { Component, OnInit, ElementRef, Renderer2, ViewChild, Output, EventEmitter } from '@angular/core';
import { ArticleService } from '../httpService/ArticleService';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { TagService } from '../httpService/TagService';
import { Tag } from '../model/tag';
import { Subscription, Subject } from 'rxjs';
import { ModalStatus } from '../article-edit-modal/article-edit-modal.component';
import { Article } from '../model/article';

@Component({
  selector: 'app-article-search',
  templateUrl: './article-search.component.html',
  styleUrls: ['./article-search.component.scss']
})
export class ArticleSearchComponent implements OnInit {
  @Output() onArticleSelected = new EventEmitter<number>();

  tagList: Tag[];
  getTagListSub: Subscription;
  maxArticleId: number;
  getMaxArticleIdSub: Subscription;


  isSearchResultVisible = false;
  isFocusedOnInput: boolean = false;
  isFocusedOnResults: boolean = false;
  isFocusedOnModal: boolean = false;
  articleSearchFormGroup: FormGroup;
  selectArticleSub: Subscription;
  searchArticleSub: Subscription;

  newArticle: Article = new Article;
  modalStatusSub: Subscription;
  modalStatus = new Subject<ModalStatus>();
  postArticleSub: Subscription;

  constructor(
    private articleService: ArticleService,
    private tagService: TagService,
  ) { }

  ngOnInit(): void {
    this.setSearchResultVisability(false);
    this.initFormGroup();
    this.fetchTagList();
    this.fetchMaxArticleId();
  }


  setSearchResultVisability(visible: boolean): void {
    this.isSearchResultVisible = visible;
  }

  private initFormGroup(): void {
    this.articleSearchFormGroup = new FormGroup({
      'id': new FormControl(null),
      'term': new FormControl(null, this.validateTerm.bind(this)),
      'results': new FormArray([]),
    });
  }

  private fetchMaxArticleId(): void {
    this.getMaxArticleIdSub = this.articleService.getAll().subscribe(
      aa => this.maxArticleId = Math.max(...aa.map(a=>a.id))
    )
  }

  getResults(): any[] {
    const results = this.articleSearchFormGroup.get('results') as FormArray;
    if(results.controls.length <= 0)
      return [];
    const res = results.controls.filter(
        r => r.get('label') !== undefined
      )
      return res;
  }

  validateTerm(termForm: FormControl): any {
    //this.setSearchResultVisability(false);
    if(!termForm.value || !this.isFocusedOnInput)
      return null;

    this.searchArticleSub?.unsubscribe();
    this.searchArticleSub = this.articleService.search(termForm.value).subscribe(
      aa => {
        let searchResultsForm = this.articleSearchFormGroup.get('results') as FormArray;
        searchResultsForm.controls = aa.map(
          a => {
            this.setSearchResultVisability(true);
            return new FormGroup({
            'id': new FormControl(a.id),
            'label': new FormControl(a.label),
            'tags': new FormArray( 
              a.tagIds.map(
                tId => {
                  const tag = this.getTag(tId);
                    return new FormGroup({
                      'id': new FormControl(tag.id),
                      'label': new FormControl(tag.label),
                      'logo': new FormControl(tag.logo),
                    });
                  }
              )
            )
          })}
        );
      }

    )
    return null;
  }
  
  onFocusUpdate(){
    console.log(`${this.isFocusedOnInput} ${this.isFocusedOnResults} ${this.isFocusedOnModal}`);
    setTimeout(
      _ => this.setSearchResultVisability(this.isFocusedOnInput || this.isFocusedOnResults || this.isFocusedOnModal)
      , 250);
  }

  fetchTagList(): void {
    this.getTagListSub?.unsubscribe();
    this.getTagListSub = this.tagService.getAll().subscribe(
      tt => this.tagList = tt
    )
  }

  getTag(id: number): Tag {
    return this.tagList.filter(t => t.id === id)[0];
  }

  getResultListStyle() {
    return {
      'opacity': this.isSearchResultVisible?
        1:
        0.2,
    };
  }

  setArticle(article: Article): void {
    this.articleSearchFormGroup = new FormGroup({
      'id': new FormControl(article.id),
      'term': new FormControl(article.label, this.validateTerm.bind(this)),
      'results': new FormArray([]),
    });
    this.isFocusedOnResults = false;
    this.setSearchResultVisability(false);
  }

  onSelectResult(articleId: number): void {
    this.selectArticleSub?.unsubscribe();
    this.selectArticleSub = this.articleService.get(articleId).subscribe(
      a => {
        this.setArticle(a);
        this.onArticleSelected.emit(a.id);
      }
    );
    
  }

  onCreateArticle(){
    const newArticleLabel = this.articleSearchFormGroup.get('term').value;
    this.newArticle = 
    {
      id: null,
      label: newArticleLabel,
      description: null,
      tagIds: [],
    } as Article;
    this.modalStatus.next(ModalStatus.Visible);
  }

  onArticleCreated(article: Article){
    console.log(article);
    this.postArticleSub?.unsubscribe();
    this.postArticleSub = this.articleService.post(article).subscribe()
  }



}
