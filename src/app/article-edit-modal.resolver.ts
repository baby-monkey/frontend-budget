import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { ItemService } from './httpService/ItemService';
import { ArticleService } from './httpService/ArticleService';
import { TagService } from './httpService/TagService';
import { CurrencyService } from './httpService/CurrencyService';

import { Observable, forkJoin, Subscription } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { Tag } from './model/tag';
import { Currency } from './model/currency';
import { Article } from './model/article';


interface ArticleEditModel {
    articleId: number,
    label: string,
    description: string,
    tagList: Tag[],
    maxArticleId: number
}


@Injectable({
  providedIn: 'root'
})
export class ArticleEditModalResolver implements Resolve<ArticleEditModel> {
    articleEditModel: ArticleEditModel;
    
    constructor(
        private articleService: ArticleService,
        private tagService: TagService,
        )
    {
    }

    resolve(
        route: ActivatedRouteSnapshot, 
        state: RouterStateSnapshot)
        : ArticleEditModel | Observable<ArticleEditModel> | Promise<ArticleEditModel> {
        
        const articleId = +route.paramMap.get('id');
        return this.fetchArticleEditModel(articleId);
    }

    // fetchArticleEditModel_alternative(articleId: number) {
    //     this.articleService.getAll().subscribe(
    //         aa => {
    //             const maxArticleId = Math.max(...aa.map(a=>a.id));
    //             let tmpArticle = {} as Article;
    //             if(articleId)
    //             tmpArticle = aa.filter(a=>a.id===articleId)[0];
    //             else
    //             tmpArticle = {id: maxArticleId + 1} as Article;
                
    //             this.articleEditModel.articleId = tmpArticle.id;
    //             this.articleEditModel.label = tmpArticle.label;
    //             this.articleEditModel.description = tmpArticle.description;
    //             this.articleEditModel.tagList = tmpArticle.tagIds.map(
    //                 ti=>{id: }
    //             );
    //             this.articleEditModel.maxArticleId = maxArticleId;
    //         }
    //     )
    // }

    fetchArticleEditModel(articleId: number): Observable<ArticleEditModel> {
        const articleEditModelObs = this.articleService.getAll().pipe(
            switchMap(
                aa => {
                    const obs = this.tagService.getAll().pipe(
                        map(
                            tt => {
                                const maxArticleId = Math.max(...aa.map(a=>a.id));
                                const ourArticle = articleId ? 
                                    aa.filter(a=>a.id === articleId)[0] : 
                                    { id: maxArticleId + 1 } as Article;
                                let articleEditModel: ArticleEditModel = 
                                {
                                    articleId: ourArticle.id,
                                    label: ourArticle.label,
                                    description: ourArticle.description,
                                    tagList: ourArticle.tagIds ?
                                        ourArticle.tagIds.map(
                                            ti => tt.filter(t=>t.id=== ti)[0]
                                        ) :
                                        [],
                                    maxArticleId: maxArticleId,
                                }
                                return articleEditModel;
                            }
                        )
                    );
                    return obs;
                }
            )
        );
            
        return articleEditModelObs;
    }

}