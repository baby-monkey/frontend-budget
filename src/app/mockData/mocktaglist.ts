import { Tag } from '../model/tag';

export const MockTagList: Tag[] = [
    {
        "id":     0,
        "label":  'Lebensmittel',
        "logo":   '🍎',
    },
    {
        "id":     1,
        "label":  'Produkte',
        "logo":   '📦',
    },
    {
        "id":     2,
        "label":  'Freizeit',
        "logo":   '🏖',
    },
    {
        "id":     3,
        "label":  'Transport',
        "logo":   '🚃',
    },
    {
        "id":     4,
        "label":  'Gemüse',
        "logo":   '🍆',
    },
];