import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { ItemService } from './httpService/ItemService';
import { ArticleService } from './httpService/ArticleService';
import { TagService } from './httpService/TagService';
import { CurrencyService } from './httpService/CurrencyService';

import { Observable, forkJoin, Subscription } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { Tag } from './model/tag';
import { Currency } from './model/currency';
import { ExpenseModel } from './expense-model';



@Injectable({
  providedIn: 'root'
})
export class ExpenseListResolver implements Resolve<ExpenseModel[]> {
    subscriptionCurrency: Subscription;
    subscriptionTag: Subscription;
    currencyList: Currency[];
    tagList: Tag[];
    
    constructor(
        private itemService: ItemService,
        private articleService: ArticleService,
        private tagService: TagService,
        private currencyService: CurrencyService
        )
    {
    }

    resolve(
        route: ActivatedRouteSnapshot, 
        state: RouterStateSnapshot)
        : ExpenseModel[] | Observable<ExpenseModel[]> | Promise<ExpenseModel[]> {
            
            this.fetchTagList();
            this.fetchCurrencyList();
            
            const res = this.itemService.getAll().pipe(
                switchMap(
                    ii => {
                        const expenseModelListObs = ii.map(
                            i => this.articleService.get(i.articleId).pipe(
                                map(
                                    a => {
                                        const tags = a.tagIds.map( tId => this.tagList.filter(t => t.id === tId)[0]);
                                        const currency = this.currencyList.filter(c => c.id === i.price.currencyId)[0];
                                        
                                        let expenseModel: ExpenseModel = {
                                            articleLabel: a.label,
                                            price: `${i.price.price} ${currency ? currency.symbol: '*'}`,
                                            tags: tags
                                        }
                                        return expenseModel;
                                    }
                                )
                            )
                        )
                        return forkJoin(expenseModelListObs);
                    }
                )
            );
            return res;
    }

    private fetchTagList() {
        this.subscriptionTag && this.subscriptionTag.unsubscribe();
        this.subscriptionTag = this.tagService.getAll().subscribe(tt => this.tagList = tt);
    }

    private fetchCurrencyList(): void {
        this.subscriptionCurrency && this.subscriptionCurrency.unsubscribe();
        this.subscriptionCurrency = this.currencyService.getAll().subscribe(cc => this.currencyList = cc);
    }

}