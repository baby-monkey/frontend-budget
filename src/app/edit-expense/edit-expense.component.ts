import { Component, OnInit } from '@angular/core';
import { ItemService } from '../httpService/ItemService';
import { ArticleService } from '../httpService/ArticleService';
import { CurrencyService } from '../httpService/CurrencyService';
import { TagService } from '../httpService/TagService';
import { Currency } from '../model/currency';
import { Event } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-edit-expense',
  templateUrl: './edit-expense.component.html',
  styleUrls: ['./edit-expense.component.scss']
})
export class EditExpenseComponent implements OnInit {
  currencyList: Currency[];

  toppings = new FormControl();
  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];

  constructor(
    private itemService: ItemService,
    private articleService: ArticleService,
    private currencyService: CurrencyService,
    private tagService: TagService,
  ) { }

  ngOnInit(): void {
    this.fetchCurrencyList();

  }

  fetchCurrencyList(): void {
    this.currencyService.getAll().subscribe(
      cc => this.currencyList = cc
    )
  }

  onArticleSelected(event: Event){
    console.log("Article Selected");
    console.log(event);
  }

}
