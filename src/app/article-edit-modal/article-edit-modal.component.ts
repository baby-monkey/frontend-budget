import { Component, OnInit, Input, Output, OnDestroy, ElementRef, Renderer2, OnChanges } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { Article } from '../model/article';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { TagService } from '../httpService/TagService';
import { ArticleService } from '../httpService/ArticleService';
import { Tag } from '../model/tag';


export enum ModalStatus {
  Hidden = 0,
  Visible = 1,
  Accepted = 2,
  Dismiss = 3,
}

@Component({
  selector: 'app-article-edit-modal',
  templateUrl: './article-edit-modal.component.html',
  styleUrls: ['./article-edit-modal.component.scss']
})
export class ArticleEditModalComponent implements OnInit, OnDestroy, OnChanges {
  @Input() article: Article = new Article;
  @Input() header?: string = "Edit Article";
  @Input() textAccept?: string = "Save";
  @Input() textDismiss?: string = "Cancle";
  @Input() status: Subject<ModalStatus> = new Subject<ModalStatus>();
  @Output() onAccept = new EventEmitter<Article>();

  private _disposableStatus: Subscription;

  tagList: Tag[];
  tagSub: Subscription;
  maxArticleId: number;
  maxArticleIdSub: Subscription;
  articleForm: FormGroup;

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2,
    private tagService: TagService,
    private articleService: ArticleService) { }

  ngOnInit(): void {
    this._initInternalStatusSubscription();
    this.setVisability(false);

    this.initForm();
    this.fetchTags();
    this.fetchMaxArticleId();
  }

  ngOnChanges(): void {
    this.fetchMaxArticleId();
    this.onUpdateArticle();
  }

  initForm() {
    this.articleForm = new FormGroup({
      'id': new FormControl(null),
      'label': new FormControl(null),
      'description': new FormControl(null),
      'tagIds': new FormArray([]),
    });
  }

  onUpdateArticle(): void {
    if(!this.article.id)
      this.article.id = this.maxArticleId + 1;
    this.articleToForm(this.article);
  }

  fetchMaxArticleId(){
    this.maxArticleIdSub = this.articleService.getAll().subscribe(
      aa => this.maxArticleId = Math.max(...aa.map(a=>a.id))
    )
  }

  setNewArticleId(article: Article): Article {
    article.id = this.maxArticleId+1;
    return article;
  }


  private _initInternalStatusSubscription(): void {
    this._disposableStatus = this.status.subscribe(
      (status) => {
        switch(+status){
          case ModalStatus.Hidden:
            this.setVisability(false);
            break;
          case ModalStatus.Visible:
            this.setVisability(true);
            break;
          case ModalStatus.Accepted:
            this.setVisability(false);
            this.onAccept.emit(this.article);
            break;
            case ModalStatus.Dismiss:
              this.setVisability(false);
              this.onAccept.emit(null);
            break;
        }
      }
    );
  }

  fetchTags() {
    this.tagSub?.unsubscribe();
    this.tagSub = this.tagService.getAll().subscribe(
      tt => this.tagList = tt      
    );
  }

  getTag(id: number): Tag {
    return this.tagList.filter(tt => tt.id === id)[0];
  }

  articleToForm(article: Article){
    console.trace(article);
    this.articleForm?.setValue(article);
  }

  formToArticle(): Article {
    const test = this.articleForm.value as Article;
    console.warn(test);
    return test;
  }

  
  setVisability(visible: boolean): void {
    console.log("ModalInfoComponent.setVisability: "+visible);
    this.renderer.setStyle(this.elementRef.nativeElement, 
      "display", 
      visible? "table": "none");
  }

  
  setAccepted(accept: boolean): void {
    console.log("ModalInfoComponent.setAccepted: " + accept);
    accept ? 
      this.status.next(ModalStatus.Accepted) :
      this.status.next(ModalStatus.Dismiss);
  }


  ngOnDestroy() {
    this._disposableStatus.unsubscribe();
  }

}
